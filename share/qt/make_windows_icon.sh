#!/bin/bash
# create multiresolution windows icon
ICON_SRC=../../src/qt/res/icons/dropcoin.png
ICON_DST=../../src/qt/res/icons/dropcoin.ico
convert ${ICON_SRC} -resize 16x16 dropcoin-16.png
convert ${ICON_SRC} -resize 32x32 dropcoin-32.png
convert ${ICON_SRC} -resize 48x48 dropcoin-48.png
convert dropcoin-16.png dropcoin-32.png dropcoin-48.png ${ICON_DST}

