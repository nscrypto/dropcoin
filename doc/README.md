Dropcoin 0.9.99 BETA
=====================

Setup
---------------------
[Dropcoin Core](https://bitbucket.org/nscrypto/dropcoin) is the original Dropcoin client and it builds the backbone of the network. However, it downloads and stores the entire history of Dropcoin transactions (which is currently several GBs); depending on the speed of your computer and network connection, the synchronization process can take anywhere from a few hours to a day or more.

Running
---------------------
The following are some helpful notes on how to run Dropcoin on your native platform. 

### Unix

You need the Qt4 run-time libraries to run Dropcoin-Qt. On Debian or Ubuntu:

	sudo apt-get install libqtgui4

Unpack the files into a directory and run:

- bin/32/dropcoin-qt (GUI, 32-bit) or bin/32/dropcoind (headless, 32-bit)
- bin/64/dropcoin-qt (GUI, 64-bit) or bin/64/dropcoind (headless, 64-bit)



### Windows

Unpack the files into a directory, and then run dropcoin-qt.exe.

### OSX

Drag Dropcoin-Qt to your applications folder, and then run Dropcoin-Qt.

### Need Help?

* Ask for help on [#dropcoin](http://webchat.freenode.net?channels=dropcoin) on Freenode. If you don't have an IRC client use [webchat here](http://webchat.freenode.net?channels=dropcoin).
* Ask for help on the [BitcoinTalk](https://bitcointalk.org/) forums, in the [Dropcoin Thread](https://bitcointalk.org/index.php?topic=1040762.0).

Building
---------------------
The following are developer notes on how to build Dropcoin on your native platform. They are not complete guides, but include notes on the necessary libraries, compile flags, etc.

- [OSX Build Notes](build-osx.md)
- [Unix Build Notes](build-unix.md)
- [Gitian Building Guide](gitian-building.md)

Development
---------------------
The Dropcoin repo's [root README](https://bitbucket.org/nscrypto/dropcoin/blob/master/README.md) contains some relevant information on the development process and automated testing.

- [Developer Notes](developer-notes.md)
- [Multiwallet Qt Development](multiwallet-qt.md)
- [Release Notes](release-notes.md)
- [Release Process](release-process.md)
- [Source Code Documentation (External Link)](https://dev.visucore.com/dropcoin/doxygen/) meh
- [Translation Process](translation_process.md)
- [Translation Strings Policy](translation_strings_policy.md)
- [Unit Tests](unit-tests.md)
- [Unauthenticated REST Interface](REST-interface.md)
- [BIPS](bips.md)

### Resources
* Discuss on the [BitcoinTalk](https://bitcointalk.org/) forums, in the [Dropcoin Thread](https://bitcointalk.org/index.php?topic=1040762.0).
* Discuss on [#dropcoin](http://webchat.freenode.net/?channels=dropcoin) on Freenode. If you don't have an IRC client use [webchat here](http://webchat.freenode.net/?channels=dropcoin).

### Miscellaneous
- [Assets Attribution](assets-attribution.md)
- [Files](files.md)
- [Tor Support](tor.md)
- [Init Scripts (systemd/upstart/openrc)](init.md)

License
---------------------
Distributed under the [MIT software license](http://www.opensource.org/licenses/mit-license.php).
This product includes software developed by the OpenSSL Project for use in the [OpenSSL Toolkit](https://www.openssl.org/). This product includes
cryptographic software written by Eric Young ([eay@cryptsoft.com](mailto:eay@cryptsoft.com)), and UPnP software written by Thomas Bernard.
