// Copyright (c) 2013 The Bitcoin Core developers
// Copyright (c) 2015 The Dropcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef DROPCOIN_NOUI_H
#define DROPCOIN_NOUI_H

extern void noui_connect();

#endif // DROPCOIN_NOUI_H
