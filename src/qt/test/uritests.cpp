// Copyright (c) 2009-2014 The Bitcoin Core developers
// Copyright (c) 2015 The Dropcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "uritests.h"

#include "guiutil.h"
#include "walletmodel.h"

#include <QUrl>

void URITests::uriTests()
{
    SendCoinsRecipient rv;
    QUrl uri;
    uri.setUrl(QString("dropcoin:DsffPY1ytB2bdRm6E2mjKgYyiXyjgKfhsC?req-dontexist="));
    QVERIFY(!GUIUtil::parseDropcoinURI(uri, &rv));

    uri.setUrl(QString("dropcoin:DsffPY1ytB2bdRm6E2mjKgYyiXyjgKfhsC?dontexist="));
    QVERIFY(GUIUtil::parseDropcoinURI(uri, &rv));
    QVERIFY(rv.address == QString("DsffPY1ytB2bdRm6E2mjKgYyiXyjgKfhsC"));
    QVERIFY(rv.label == QString());
    QVERIFY(rv.amount == 0);

    uri.setUrl(QString("dropcoin:DsffPY1ytB2bdRm6E2mjKgYyiXyjgKfhsC?label=Example Address"));
    QVERIFY(GUIUtil::parseDropcoinURI(uri, &rv));
    QVERIFY(rv.address == QString("DsffPY1ytB2bdRm6E2mjKgYyiXyjgKfhsC"));
    QVERIFY(rv.label == QString("Wikipedia Example Address"));
    QVERIFY(rv.amount == 0);

    uri.setUrl(QString("dropcoin:DsffPY1ytB2bdRm6E2mjKgYyiXyjgKfhsC?amount=0.001"));
    QVERIFY(GUIUtil::parseDropcoinURI(uri, &rv));
    QVERIFY(rv.address == QString("DsffPY1ytB2bdRm6E2mjKgYyiXyjgKfhsC"));
    QVERIFY(rv.label == QString());
    QVERIFY(rv.amount == 100000);

    uri.setUrl(QString("dropcoin:DsffPY1ytB2bdRm6E2mjKgYyiXyjgKfhsC?amount=1.001"));
    QVERIFY(GUIUtil::parseDropcoinURI(uri, &rv));
    QVERIFY(rv.address == QString("DsffPY1ytB2bdRm6E2mjKgYyiXyjgKfhsC"));
    QVERIFY(rv.label == QString());
    QVERIFY(rv.amount == 100100000);

    uri.setUrl(QString("dropcoin:DsffPY1ytB2bdRm6E2mjKgYyiXyjgKfhsC?amount=100&label=Example"));
    QVERIFY(GUIUtil::parseDropcoinURI(uri, &rv));
    QVERIFY(rv.address == QString("DsffPY1ytB2bdRm6E2mjKgYyiXyjgKfhsC"));
    QVERIFY(rv.amount == 10000000000LL);
    QVERIFY(rv.label == QString("Wikipedia Example"));

    uri.setUrl(QString("dropcoin:DsffPY1ytB2bdRm6E2mjKgYyiXyjgKfhsC?message=Example Address"));
    QVERIFY(GUIUtil::parseDropcoinURI(uri, &rv));
    QVERIFY(rv.address == QString("DsffPY1ytB2bdRm6E2mjKgYyiXyjgKfhsC"));
    QVERIFY(rv.label == QString());

    QVERIFY(GUIUtil::parseDropcoinURI("dropcoin://DsffPY1ytB2bdRm6E2mjKgYyiXyjgKfhsC?message=Example Address", &rv));
    QVERIFY(rv.address == QString("DsffPY1ytB2bdRm6E2mjKgYyiXyjgKfhsC"));
    QVERIFY(rv.label == QString());

    uri.setUrl(QString("dropcoin:DsffPY1ytB2bdRm6E2mjKgYyiXyjgKfhsC?req-message=Example Address"));
    QVERIFY(GUIUtil::parseDropcoinURI(uri, &rv));

    uri.setUrl(QString("dropcoin:DsffPY1ytB2bdRm6E2mjKgYyiXyjgKfhsC?amount=1,000&label=Example"));
    QVERIFY(!GUIUtil::parseDropcoinURI(uri, &rv));

    uri.setUrl(QString("dropcoin:DsffPY1ytB2bdRm6E2mjKgYyiXyjgKfhsC?amount=1,000.0&label=Example"));
    QVERIFY(!GUIUtil::parseDropcoinURI(uri, &rv));
}
