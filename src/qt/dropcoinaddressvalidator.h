// Copyright (c) 2011-2014 The Bitcoin Core developers
// Copyright (c) 2015 The Dropcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef DROPCOIN_QT_DROPCOINADDRESSVALIDATOR_H
#define DROPCOIN_QT_DROPCOINADDRESSVALIDATOR_H

#include <QValidator>

/** Base58 entry widget validator, checks for valid characters and
 * removes some whitespace.
 */
class DropcoinAddressEntryValidator : public QValidator
{
    Q_OBJECT

public:
    explicit DropcoinAddressEntryValidator(QObject *parent);

    State validate(QString &input, int &pos) const;
};

/** Dropcoin address widget validator, checks for a valid dropcoin address.
 */
class DropcoinAddressCheckValidator : public QValidator
{
    Q_OBJECT

public:
    explicit DropcoinAddressCheckValidator(QObject *parent);

    State validate(QString &input, int &pos) const;
};

#endif // DROPCOIN_QT_DROPCOINADDRESSVALIDATOR_H
