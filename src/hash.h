// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2013 The Bitcoin Core developers
// Copyright (c) 2015 The Dropcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef DROPCOIN_HASH_H
#define DROPCOIN_HASH_H

#include "crypto/ripemd160.h"
#include "crypto/sha256.h"
#include "serialize.h"
#include "uint256.h"
#include "old_uint256.h"
#include "version.h"

#include "sph_blake.h"
#include "sph_groestl.h"
#include "sph_jh.h"
#include "sph_keccak.h"
#include "sph_skein.h"
#include "sph_cubehash.h"
#include "sph_echo.h"
#include "sph_fugue.h"
#include "sph_luffa.h"
#include "sph_simd.h"
#include "sph_shavite.h"

#include <vector>

typedef uint256 ChainCode;

/** 256-bit hasher class (double SHA-256). */
class CHash256 {
private:
    CSHA256 sha;
public:
    static const size_t OUTPUT_SIZE = CSHA256::OUTPUT_SIZE;

    void Finalize(unsigned char hash[OUTPUT_SIZE]) {
        unsigned char buf[sha.OUTPUT_SIZE];
        sha.Finalize(buf);
        sha.Reset().Write(buf, sha.OUTPUT_SIZE).Finalize(hash);
    }

    CHash256& Write(const unsigned char *data, size_t len) {
        sha.Write(data, len);
        return *this;
    }

    CHash256& Reset() {
        sha.Reset();
        return *this;
    }
};


/** 160-bit hasher class (SHA-256 + RIPEMD-160). */
class CHash160 {
private:
    CSHA256 sha;
public:
    static const size_t OUTPUT_SIZE = CRIPEMD160::OUTPUT_SIZE;

    void Finalize(unsigned char hash[OUTPUT_SIZE]) {
        unsigned char buf[sha.OUTPUT_SIZE];
        sha.Finalize(buf);
        CRIPEMD160().Write(buf, sha.OUTPUT_SIZE).Finalize(hash);
    }

    CHash160& Write(const unsigned char *data, size_t len) {
        sha.Write(data, len);
        return *this;
    }

    CHash160& Reset() {
        sha.Reset();
        return *this;
    }
};

/** Compute the 256-bit hash of an object. */
template<typename T1>
inline uint256 Hash(const T1 pbegin, const T1 pend)
{
    static const unsigned char pblank[1] = {};
    uint256 result;
    CHash256().Write(pbegin == pend ? pblank : (const unsigned char*)&pbegin[0], (pend - pbegin) * sizeof(pbegin[0]))
              .Finalize((unsigned char*)&result);
    return result;
}

/** Compute the 256-bit hash of the concatenation of two objects. */
template<typename T1, typename T2>
inline uint256 Hash(const T1 p1begin, const T1 p1end,
                    const T2 p2begin, const T2 p2end) {
    static const unsigned char pblank[1] = {};
    uint256 result;
    CHash256().Write(p1begin == p1end ? pblank : (const unsigned char*)&p1begin[0], (p1end - p1begin) * sizeof(p1begin[0]))
              .Write(p2begin == p2end ? pblank : (const unsigned char*)&p2begin[0], (p2end - p2begin) * sizeof(p2begin[0]))
              .Finalize((unsigned char*)&result);
    return result;
}

/** Compute the 256-bit hash of the concatenation of three objects. */
template<typename T1, typename T2, typename T3>
inline uint256 Hash(const T1 p1begin, const T1 p1end,
                    const T2 p2begin, const T2 p2end,
                    const T3 p3begin, const T3 p3end) {
    static const unsigned char pblank[1] = {};
    uint256 result;
    CHash256().Write(p1begin == p1end ? pblank : (const unsigned char*)&p1begin[0], (p1end - p1begin) * sizeof(p1begin[0]))
              .Write(p2begin == p2end ? pblank : (const unsigned char*)&p2begin[0], (p2end - p2begin) * sizeof(p2begin[0]))
              .Write(p3begin == p3end ? pblank : (const unsigned char*)&p3begin[0], (p3end - p3begin) * sizeof(p3begin[0]))
              .Finalize((unsigned char*)&result);
    return result;
}

/** Compute the 160-bit hash an object. */
template<typename T1>
inline uint160 Hash160(const T1 pbegin, const T1 pend)
{
    static unsigned char pblank[1] = {};
    uint160 result;
    CHash160().Write(pbegin == pend ? pblank : (const unsigned char*)&pbegin[0], (pend - pbegin) * sizeof(pbegin[0]))
              .Finalize((unsigned char*)&result);
    return result;
}

/** Compute the 160-bit hash of a vector. */
inline uint160 Hash160(const std::vector<unsigned char>& vch)
{
    return Hash160(vch.begin(), vch.end());
}

/** A writer stream (for serialization) that computes a 256-bit hash. */
class CHashWriter
{
private:
    CHash256 ctx;

public:
    int nType;
    int nVersion;

    CHashWriter(int nTypeIn, int nVersionIn) : nType(nTypeIn), nVersion(nVersionIn) {}

    CHashWriter& write(const char *pch, size_t size) {
        ctx.Write((const unsigned char*)pch, size);
        return (*this);
    }

    // invalidates the object
    uint256 GetHash() {
        uint256 result;
        ctx.Finalize((unsigned char*)&result);
        return result;
    }

    template<typename T>
    CHashWriter& operator<<(const T& obj) {
        // Serialize to this stream
        ::Serialize(*this, obj, nType, nVersion);
        return (*this);
    }
};

/** Compute the 256-bit hash of an object's serialization. */
template<typename T>
uint256 SerializeHash(const T& obj, int nType=SER_GETHASH, int nVersion=PROTOCOL_VERSION)
{
    CHashWriter ss(nType, nVersion);
    ss << obj;
    return ss.GetHash();
}

unsigned int MurmurHash3(unsigned int nHashSeed, const std::vector<unsigned char>& vDataToHash);

void BIP32Hash(const ChainCode &chainCode, unsigned int nChild, unsigned char header, const unsigned char data[32], unsigned char output[64]);


/* ----------- Dropcoin Hash ------------------------------------------------ */

inline void switchHash(const void *input, void *output, int id)
{
    switch(id) {
        case 0:
            sph_keccak512_context ctx_keccak;
            sph_keccak512_init(&ctx_keccak);
            sph_keccak512(&ctx_keccak, input, 64);
            sph_keccak512_close(&ctx_keccak, output);
            break;
        case 1:
            sph_blake512_context ctx_blake;
            sph_blake512_init(&ctx_blake);
            sph_blake512(&ctx_blake, input, 64);
            sph_blake512_close(&ctx_blake, output);
            break;
        case 2:
            sph_groestl512_context ctx_groestl;
            sph_groestl512_init(&ctx_groestl);
            sph_groestl512(&ctx_groestl, input, 64);
            sph_groestl512_close(&ctx_groestl, output);
            break;
        case 3:
            sph_skein512_context ctx_skein;
            sph_skein512_init(&ctx_skein);
            sph_skein512(&ctx_skein, input, 64);
            sph_skein512_close(&ctx_skein, output);
            break;
        case 4:
            sph_luffa512_context ctx_luffa;
            sph_luffa512_init(&ctx_luffa);
            sph_luffa512(&ctx_luffa, input, 64);
            sph_luffa512_close(&ctx_luffa, output);
            break;
        case 5:
            sph_echo512_context ctx_echo;
            sph_echo512_init(&ctx_echo);
            sph_echo512(&ctx_echo, input, 64);
            sph_echo512_close(&ctx_echo, output);
            break;
        case 6:
            sph_shavite512_context ctx_shavite;
            sph_shavite512_init(&ctx_shavite);
            sph_shavite512(&ctx_shavite, input, 64);
            sph_shavite512_close(&ctx_shavite, output);
            break;
        case 7:
            sph_fugue512_context ctx_fugue;
            sph_fugue512_init(&ctx_fugue);
            sph_fugue512(&ctx_fugue, input, 64);
            sph_fugue512_close(&ctx_fugue, output);
            break;
        case 8:
            sph_simd512_context ctx_simd;
            sph_simd512_init(&ctx_simd);
            sph_simd512(&ctx_simd, input, 64);
            sph_simd512_close(&ctx_simd, output);
            break;
        case 9:
            sph_cubehash512_context ctx_cubehash;
            sph_cubehash512_init(&ctx_cubehash);
            sph_cubehash512(&ctx_cubehash, input, 64);
            sph_cubehash512_close(&ctx_cubehash, output);
            break;
        default:
            break;
    }
}


template<typename T1>
inline olduint::uint512 HashLP(const T1 pbegin, const T1 pend)
{
    sph_jh512_context ctx_jh;
    static unsigned char pblank[1];
    pblank[0] = 0;
    olduint::uint512 hash[2];

    const void *pStart = (pbegin == pend ? pblank : static_cast<const void*>(&pbegin[0]));
    size_t nSize = (pend - pbegin) *sizeof(pbegin[0]);

    sph_jh512_init(&ctx_jh);
    sph_jh512(&ctx_jh, pStart, nSize);
    sph_jh512_close(&ctx_jh, static_cast<void*>(&hash[0]));

    int startPosition = hash[0].getinnerint(0) % 31;

    for (int i = startPosition; i < 31; i--) {
        int start = i % 10;
        for (int j = start; j < 10; j++) {
            hash[1] = hash[0] << (i % 4);
            switchHash((const void*)&hash[1], (void*)&hash[0], j);
        }
        for (int j = 0; j < start; j++) {
            hash[1] = hash[0] << (i % 4);
            switchHash((const void*)&hash[1], (void*)&hash[0], j);
        }
        i += 10;
    }
    for (int i = 0; i < startPosition; i--) {
        int start = i % 10;
        for (int j = start; j < 10; j++) {
            hash[1] = hash[0] << (i % 4);
            switchHash((const void*)&hash[1], (void*)&hash[0], j);
        }
        for (int j = 0; j < start; j++) {
            hash[1] = hash[0] << (i % 4);
            switchHash((const void*)&hash[1], (void*)&hash[0], j);
        }
        i += 10;
    }

    return hash[0];
}

#endif
