// Copyright (c) 2010 Satoshi Nakamoto
// Copyright (c) 2009-2014 The Bitcoin Core developers
// Copyright (c) 2015 The Dropcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "chainparams.h"

#include "arith_uint256.h"
#include "util.h"
#include "utilstrencodings.h"

#include <assert.h>

#include <boost/assign/list_of.hpp>

using namespace std;

#include "chainparamsseeds.h"

/**
 * Main network
 */
/**
 * What makes a good checkpoint block?
 * + Is surrounded by blocks with reasonable timestamps
 *   (no blocks before with a timestamp after, none after with
 *    timestamp before)
 * + Contains no strange transactions
 */

class CMainParams : public CChainParams {
public:
    CMainParams() {

        strNetworkID = "main";
        consensus.nSubsidyHalvingInterval = 172328;
        consensus.nMajorityEnforceBlockUpgrade = 7500;
        consensus.nMajorityRejectBlockOutdated = 9500;
        consensus.nMajorityWindow = 10000;
        consensus.powLimit = uint256S("0x00003fffffffffffffffffffffffffffffffffffffffffffffffffffffffffff");
        consensus.nPowTargetSpacing = 61;
        consensus.nPowTargetTimespan = 4 * consensus.nPowTargetSpacing; // ~4mins
        consensus.fPowAllowMinDifficultyBlocks = false;

        /**
         * The message start string is designed to be unlikely to occur in normal data.
         * The characters are rarely used upper ASCII, not valid as UTF-8, and produce
         * a large 32-bit integer with any alignment.
         */
        pchMessageStart[0] = 0x9e;
        pchMessageStart[1] = 0xee;
        pchMessageStart[2] = 0x83;
        pchMessageStart[3] = 0x2b;
        vAlertPubKey = ParseHex("045125a5731d896b7b0fe1dce34ae065066a83bb05fa311c4e434d77ed65e50f2d34c985abbf1607f797390dc735d5d6d4bbedb04538da97e956e303b2912b22c2");
        nDefaultPort = 29029;
        nMinerThreads = 0;
        nPruneAfterHeight = 100000;


        const char* pszTimestamp = "May 5, 2015 - IS says it was behind US Prophet cartoon attack - http://www.bbc.com/news/world-us-canada-32589546";
        CMutableTransaction txNew;

        // Set the input of the coinbase
        txNew.vin.resize(1);
        txNew.vin[0].scriptSig = CScript() << 0 << CScriptNum(42) << vector<unsigned char>((const unsigned char*)pszTimestamp, (const unsigned char*)pszTimestamp + strlen(pszTimestamp));

        // Set the output of the coinbase
        txNew.vout.resize(1);
        txNew.vout[0].nValue = 0;
        genesis.vtx.push_back(txNew);
        genesis.hashPrevBlock.SetNull();
        genesis.nVersion          = 1;
        genesis.SetPoKFlag(true);
        genesis.SetPoK(0);
        genesis.nNonce            = 26514;
        genesis.nTime             = 1430836487;
        genesis.nBits             = 0x1E3FFFFF;
        genesis.hashMerkleRoot    = genesis.BuildMerkleTree();
        genesis.SetPoK(genesis.CalculatePoK());

        consensus.hashGenesisBlock = genesis.GetHash();
        assert(consensus.hashGenesisBlock == uint256S("0x00003fdb4b1445d95583d1045fd311d88d2496aa1f491107a347086936be18a8"));
        assert(genesis.hashMerkleRoot == uint256S("0x1763f51dbe42940642fb7db11f4b89294d5595e67dfd501bb504da3248fe3d84"));


        vSeeds.push_back(CDNSSeedData("dnsseed.nscrypto.ninja", "dnsseed.nscrypto.ninja"));
        vSeeds.push_back(CDNSSeedData("dnsseed.nscrypto.j43.ca", "dnsseed.nscrypto.j43.ca"));
        vSeeds.push_back(CDNSSeedData("node1.crypto.drop.software", "node1.crypto.drop.software"));

        base58Prefixes[PUBKEY_ADDRESS] = boost::assign::list_of(31);
        base58Prefixes[SCRIPT_ADDRESS] = boost::assign::list_of(10);
        base58Prefixes[SECRET_KEY] =     boost::assign::list_of(159);
        base58Prefixes[EXT_PUBLIC_KEY] = boost::assign::list_of(0x04)(0x88)(0xB2)(0x1E).convert_to_container<std::vector<unsigned char> >();
        base58Prefixes[EXT_SECRET_KEY] = boost::assign::list_of(0x04)(0x88)(0xAD)(0xE4).convert_to_container<std::vector<unsigned char> >();

        //vFixedSeeds = std::vector<SeedSpec6>(pnSeed6_main, pnSeed6_main + ARRAYLEN(pnSeed6_main));

        fRequireRPCPassword = true;
        fMiningRequiresPeers = true;
        fDefaultConsistencyChecks = false;
        fRequireStandard = true;
        fMineBlocksOnDemand = false;
        fTestnetToBeDeprecatedFieldRPC = false;

        checkpointData = (Checkpoints::CCheckpointData) {
            boost::assign::map_list_of
            (0,    uint256S("0x00003fdb4b1445d95583d1045fd311d88d2496aa1f491107a347086936be18a8"))
            (1,    uint256S("0x000012aa1b524000c8b0cbf5304a713e705347bc5efa62fe9a75a0b958627f71"))
            (77,   uint256S("0x000001668424013da4d25197fa12cebd9883b1195edaf65431e82c4418db06a3"))
            (1337, uint256S("0x00000003a63c5507359e09dc61ea474fcbdbe3606a836c18b03d32b5d3bc6f0c"))
            (7866, uint256S("0x0000000bd004504b650e886b18ccbd91c496bb71d9fa2fd4aff2ed24ec18063b")),
            1433315860, // * UNIX timestamp of last checkpoint block
            8265,       // * total number of transactions between genesis and last checkpoint
            1480        // * estimated number of transactions per day after checkpoint
        };
    }
};
static CMainParams mainParams;

/**
 * Testnet (v2)
 */
class CTestNetParams : public CMainParams {
public:
    CTestNetParams() {
        strNetworkID = "test";
        consensus.nMajorityEnforceBlockUpgrade = 721;
        consensus.nMajorityRejectBlockOutdated = 855;
        consensus.nMajorityWindow = 1440;
        consensus.powLimit = uint256S("0x0000ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff");
        consensus.fPowAllowMinDifficultyBlocks = true;

        // The message start string is designed to be unlikely to occur in normal data.
        // The characters are rarely used upper ASCII, not valid as UTF-8, and produce
        // a large 4-byte int at any alignment.
        pchMessageStart[0] = 0x8b;
        pchMessageStart[1] = 0x11;
        pchMessageStart[2] = 0x09;
        pchMessageStart[3] = 0x06;
        nDefaultPort = 29028;
        nMinerThreads = 0;
        nPruneAfterHeight = 1000;

        //! Modify the testnet genesis block so the timestamp is valid for a later start.
        genesis.nTime = 1430836488;
        genesis.nNonce = 43952;
        genesis.nBits = 0x1F00FFFF;
        genesis.SetPoK(genesis.CalculatePoK());
        consensus.hashGenesisBlock = genesis.GetHash();

        assert(consensus.hashGenesisBlock == uint256S("0x00007ec8fb6ba554aff46a958f8515f1c9ee2675a2de59ffbc7ffbd0477aa052"));
        // Merkle root is the same as parent

//        vFixedSeeds.clear();
        vSeeds.clear();
//        vSeeds.push_back(CDNSSeedData("0.0.0.0", "0.0.0.0"));

        base58Prefixes[PUBKEY_ADDRESS] = boost::assign::list_of(111);
        base58Prefixes[SCRIPT_ADDRESS] = boost::assign::list_of(196);
        base58Prefixes[SECRET_KEY] =     boost::assign::list_of(239);
        base58Prefixes[EXT_PUBLIC_KEY] = boost::assign::list_of(0x04)(0x35)(0x87)(0xCF);
        base58Prefixes[EXT_SECRET_KEY] = boost::assign::list_of(0x04)(0x35)(0x83)(0x94);


//    vFixedSeeds = std::vector<SeedSpec6>(pnSeed6_test, pnSeed6_test + ARRAYLEN(pnSeed6_test));

    fRequireRPCPassword = true;
    fMiningRequiresPeers = true;
    fDefaultConsistencyChecks = false;
    fRequireStandard = false;
    fMineBlocksOnDemand = false;
    fTestnetToBeDeprecatedFieldRPC = true;

    checkpointData = (Checkpoints::CCheckpointData) {
        boost::assign::map_list_of
            (0,    uint256S("0x00007ec8fb6ba554aff46a958f8515f1c9ee2675a2de59ffbc7ffbd0477aa052"))
            (313,  uint256S("0x0000085bffde4edb4591e38cada30ceec06caf0ba0c82e93f7e0e55cbb471c1f"))
            (9507, uint256S("0x0000d7d92ff8643bd982fe55643d73ed1e6573a97300ff78e283c185ebd2532c")),
            1432677339,
            9528,
            1
    };

}
};
static CTestNetParams testNetParams;

/**
 * Regression test
 */
class CRegTestParams : public CTestNetParams {
public:
    CRegTestParams() {
        strNetworkID = "regtest";
        consensus.nSubsidyHalvingInterval = 1500;
        consensus.nMajorityEnforceBlockUpgrade = 7500;
        consensus.nMajorityRejectBlockOutdated = 9500;
        consensus.nMajorityWindow = 10000;
        consensus.powLimit = uint256S("0x7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff");
        pchMessageStart[0] = 0xda;
        pchMessageStart[1] = 0xfb;
        pchMessageStart[2] = 0xb5;
        pchMessageStart[3] = 0xad;
        nDefaultPort = 29129;
        nMinerThreads = 1;

        genesis.nBits             = UintToArith256(consensus.powLimit).GetCompact();
        genesis.nTime             = 1430836489;
        genesis.nNonce            = 1;
        genesis.SetPoK(genesis.CalculatePoK());

        consensus.hashGenesisBlock = genesis.GetHash();
        assert(consensus.hashGenesisBlock == uint256S("0x3d7ef11779f23a6ebd935971e14345c8ff4f26bba3510868b2366437913d1c1d"));
        nPruneAfterHeight = 10000;


//        printf("Genesis main: \n nBits=%08x \n powLimit=%s \n pok=%u OR 0x%08x \n\n", genesis.nBits, consensus.powLimit.GetHex().c_str(), genesis.GetPoK(), genesis.GetPoK());

//        vFixedSeeds.clear(); //! Regtest mode doesn't have any fixed seeds.
        vSeeds.clear();  //! Regtest mode doesn't have any DNS seeds.

        fRequireRPCPassword = false;
        fMiningRequiresPeers = false;
        fDefaultConsistencyChecks = true;
        fRequireStandard = false;
        fMineBlocksOnDemand = true;
        fTestnetToBeDeprecatedFieldRPC = false;

        checkpointData = (Checkpoints::CCheckpointData) {
            boost::assign::map_list_of
            ( 0, uint256S("0x0x3d7ef11779f23a6ebd935971e14345c8ff4f26bba3510868b2366437913d1c1d")),
            0,
            0,
            0
        };
    }
};
static CRegTestParams regTestParams;

static CChainParams *pCurrentParams = 0;

const CChainParams &Params() {
    assert(pCurrentParams);
    return *pCurrentParams;
}

CChainParams &Params(CBaseChainParams::Network network) {
    switch (network) {
        case CBaseChainParams::MAIN:
            return mainParams;
        case CBaseChainParams::TESTNET:
            return testNetParams;
        case CBaseChainParams::REGTEST:
            return regTestParams;
        default:
            assert(false && "Unimplemented network");
            return mainParams;
    }
}

void SelectParams(CBaseChainParams::Network network) {
    SelectBaseParams(network);
    pCurrentParams = &Params(network);
}

bool SelectParamsFromCommandLine()
{
    CBaseChainParams::Network network = NetworkIdFromCommandLine();
    if (network == CBaseChainParams::MAX_NETWORK_TYPES)
        return false;

    SelectParams(network);
    return true;
}
