=======
Dropcoin Core integration/staging tree
=====================================


License
-------

Dropcoin Core is released under the terms of the MIT license. See [COPYING](COPYING) for more
information or see http://opensource.org/licenses/MIT.
